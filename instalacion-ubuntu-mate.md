# Instalación de Ubuntu Mate en Raspberry Pi 3


Descargamos una imagen de Debian Mate desde la pagina oficial de [Raspberry](https://ubuntu-mate.org/raspberry-pi/).

Siguiendo los paso indicados en la guía procedemos a realizar la instalación. 

La memoria donde vamos a instalar es una Micro SD marca Kingston de 16Gb [clase 10](http://www.kingston.com/latam/flash/microsd_cards/sdc10g2)

 1. Verificamos la tarjeta:
 

	    $ df -h
	    S.ficheros     Tamaño Usados  Disp Uso% Montado en
	    udev             2,8G      0  2,8G   0% /dev
	    ......
	    /dev/sdc1         15G    32K   15G   1% /media/yamila/5B4A-3526



 2. Ejecutamos:

		$sudo apt-get install gddrescue xz-utils
		$unxz ubuntu-mate-16.04-desktop-armhf-raspberry-pi.img.xz
		$sudo ddrescue -D --force ubuntu-mate-16.04-desktop-armhf-raspberry-pi.img /dev/sdc

    Va a demorar unos cuantos minutos:
 
        GNU ddrescue 1.19
        Press Ctrl-C to interrupt
        rescued:     8053 MB,  errsize:       0 B,  current rate:    3407 kB/s
           ipos:     8052 MB,   errors:       0,    average rate:   10104 kB/s
           opos:     8052 MB, run time:   13.28 m,  successful read:       0 s ago
        Finished 
 

 
 
    IMPORTANTE: Poner sdx, y no sdx1. 

 3. Retirar la tarjeta SD
 