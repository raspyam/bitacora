# Instalación de Raspbian en Raspberry Pi 3


Descargamos una imagen de Raspbian desde la pagina oficial de [Raspberry](https://www.raspberrypi.org/downloads/raspbian/).

		RASPBIAN JESSIE
		Full desktop image based on Debian Jessie


Siguiendo los paso indicados en la guía procedemos a realizar la instalación. 

La memoria donde vamos a instalar es una Micro SD marca Kingston de 16Gb [clase 10](http://www.kingston.com/latam/flash/microsd_cards/sdc10g2)

 1. Verificamos la tarjeta:
 

	    yamila@yamila-np530:~/raspberry$ df -h
	    S.ficheros     Tamaño Usados  Disp Uso% Montado en
	    udev             2,8G      0  2,8G   0% /dev
	    tmpfs            575M   9,0M  566M   2% /run
	    /dev/sdb1         22G   7,1G   14G  35% /
	    tmpfs            2,9G    42M  2,8G   2% /dev/shm
	    tmpfs            5,0M   4,0K  5,0M   1% /run/lock
	    tmpfs            2,9G      0  2,9G   0% /sys/fs/cgroup
	    /dev/sda7        255G    77G  166G  32% /home
	    /dev/sda6         28G   1,4G   26G   6% /var
	    cgmfs            100K      0  100K   0% /run/cgmanager/fs
	    tmpfs            575M    40K  575M   1% /run/user/1000
	    /dev/sdc1         15G    32K   15G   1% /media/yamila/5B4A-3526

 2. Desmotamos la tarjeta SD:
			 
		$ umount /dev/sdc1

 3. Extraer la imagen contenida en __2016-05-27-raspbian-jessie.zip__
 
 
 4. Ejecutar el comando dd

		$ sudo dd bs=4M if=2016-05-27-raspbian-jessie.img of=/dev/sdc


    	yamila@yamila-np530:~/raspberry$ sudo dd bs=4M if=2016-05-27-raspbian-jessie.img of=/dev/sdc
        958+1 registros leídos
        958+1 registros escritos
        4019191808 bytes (4,0 GB, 3,7 GiB) copied, 459,339 s, 8,7 MB/s

 5. Ejecutar el comando sync para asegurar que la cache quede vacia y poder desmontar con seguridad la tarjeta

        $ sync

 
 6. Retirar la tarjeta SD
 

 
# Instalación de Raspbian en Raspberry Pi 3



